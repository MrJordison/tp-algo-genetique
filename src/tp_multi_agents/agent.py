# coding:utf-8

import igraph, random

#Classe représentant une fourmi évoluant sur le graphe
#dans la représentation du modèle multi-agents
class Agent:

    #Constructeur d'une instance d'agent, prend en paramètre
    #le graphe, afin de choisir aléatoirement un noeud
    #de départ
    #@param graphe : le graphe sur lequel évoluera l'agent
    def __init__(self, graphe):
        #Environnement dans lequel évolue l'agent
        #ici, le graphe avec ses nodes et arêtes
        self.environnement = graphe

        #Noeud courant sur lequel se positionne la fourmi
        self.position_node = random.randint(0,len(graphe.vs)-1)
 
        #Liste des noeuds déjà visités
        self.visited_nodes = [self.position_node]

    #Constructeur de la classe Agent, prenant en paramètre le graphe et un vertice de départ sur ce dernier
    #@param graphe:  le graphe sur lequel évoluera l'agent
    #@param position : l'indice du vertice de départ sur lequel démarrera l'agent
    """
    def __init__(self, graphe, position):
        #Environnement dans lequel évolue l'agent
        #ici, le graphe avec ses nodes et arêtes
        self.environnement = graphe

        #Noeud courant sur lequel se positionne la fourmi
        self.position_node = position
 
        #Liste des noeuds déjà visités
        self.visited_nodes = [self.position_node]
    """

    #Méthode renvoyant les arcs que peut emprunter l'agent depuis sa position. Les arcs emmènent obligatoirement vers des noeuds non visités, les autres ramenant en arrière sur le circuit ou créant un cycle sont ignorés.
    #@return la liste d'edges du graphe que peut emprunter l'agent
    def visibility(self):
        #Récupération de tous les noeuds accessibles depuis
        #le noeud courant (noeuds non visités)
        nodes_atteignables = []
        index = 0
        for edge in self.environnement.es.select(_source=self.position_node):
            if edge.target not in self.visited_nodes:
                nodes_atteignables +=[edge]

        for edge in self.environnement.es.select(_target=self.position_node):
            if edge.source not in self.visited_nodes:
                nodes_atteignables +=[edge]
        return nodes_atteignables
 
    #Méthode qui fait parcourir un arc à l'agent et l'emmène vers un nouveau noeud du graphe non visité. Le choix est fait de mamanière aléatoire (roulette russe), dont la pondération repose sur la coût minimum de l'arc et de la quantité maximum de phéromones déposée. Après la décision, la mémoire de la fourmi est mise à jour ainsi que sa position
    def mouvement(self):
        #récupération des arcs empruntables par la fourmis
        edges_atteignables = self.visibility()

        #Dans le cas où la fourmi n'a plus d'arcs visibles, cela signifie
        #qu'elle a fini de parcourir son chemin
        if len(edges_atteignables) == 0 :
            print("La fourmi a déjà fini de parcourir tous les noeuds !")
        #sinon
        else:
            #Cas où il y a plusieurs arcs empruntables
            if len(edges_atteignables) != 1 :
 
                #Somme de l'ensemble des couts des arcs possibles
                cost_total = 0
                for edge in edges_atteignables:
                    cost_total+=edge["cost"]
 

                ponderation_sum = 0
                ponderation = []
                #Calcul de la pondération cout/phéromone de chaque arc
                #et de la somme de toutes ces pondérations
                for edge in edges_atteignables:
                    ponderation += [(cost_total - edge["cost"]) * edge["pheromones"]]
                    ponderation_sum += ponderation[-1]
 
                #roulette russe pour choisir une arête aléatoirement en fonction
                #de la pondération
                tirage = random.randint(1, ponderation_sum)
 
                tranche = 0
                index_choix = -1
                for i in range(len(ponderation)):
                    tranche+=ponderation[i]
                    if tirage <=tranche:
                        index_choix = i
                        break

                #récupération de l'arc choisi
                edge_choisi = edges_atteignables[index_choix]

            #cas où il ne reste qu'un arc à parcourir
            else:
                edge_choisi = edges_atteignables[0]

            next_vertice = edge_choisi.source if edge_choisi.source != self.position_node else edge_choisi.target

            #mise à jour de la mémoire de l'agent fourmi et changement
            #de sa position avec le noeud choisi
            self.visited_nodes +=[next_vertice]
            self.position_node = next_vertice
 

    #Méthode qui réinitialise la mémoire de la fourmi, la ramène à sa position de départ, tout en déposant des phéromones sur les arcs qu'elles a parcourue. Renvoie le cout total du chemin ainsi que l'ordre des noeuds visités
    #return : un 2-uplet contenant la liste des noeuds visités et le coût du chemin
    def backtrack_n_reset(self):
        #initialisation du coût du chemin à retourner
        result = 0
        chemin = self.visited_nodes

        #parcours de tous les arcs du circuit
        for i in range(len(chemin)-1):
            edge = self.environnement.es.select(_within=[chemin[i],chemin[i+1]])
            #mise à jour du coût total du circuit
            result+= edge["cost"][0]
            #mise à jour des phéromones de l'arc courant
            edge["pheromones"][0]+=1

        #reset de la mémoire de fourmi et repositionnement au point de départ
        self.position_node = self.visited_nodes[0]
        self.visited_nodes = [self.position_node]
 
        #retourne le chemin
        return (chemin,result)
