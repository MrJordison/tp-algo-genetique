#!/usr/bin/env python3
# coding: utf-8

import igraph
import random
from .agent import Agent


def main():
    run(5, 6, 5, 1)


# Méthode pour générer un graphe d'après un nombre de vertices prédéfinis
# initialise un arc entre chaque vertice avec un coût aléatoire compris
# entre [1;max] prédéfini
# et une quantité de phéromone arbitraire
# @param nb_vertices : nombre de sommets constituant le graphe
# @param pherom_quantity : la quantité de phéromones de départ sur chaque arc
# @param max : la valeur max pour générer aléatoirement le coût d'un arc
# @return : le graphe généré
def generate_graphe(nb_vertices, pherom_quantity, max):
    # création du graphe et du nombre de vertices le composant
    graphe = igraph.Graph(directed=False)
    graphe.add_vertices(nb_vertices)

    # Ajout d'arc entre tous les vertices du graphes
    for i in range(nb_vertices):
        for j in range(i+1, nb_vertices):
            graphe.add_edges([(i, j)])

    # setup de la quantité de phéromones de départ
    # et d'un coût aléatoire pour chaque arc
    for i in range(0, len(graphe.es)):
        graphe.es[i]["cost"] = random.randint(1, max)
        graphe.es[i]["pheromones"] = pherom_quantity

    return graphe


def setup_graphe(graphe, pherom_quantity, max):
    # setup de la quantité de phéromones de départ
    # et d'un coût aléatoire pour chaque arc
    for i in range(0, len(graphe.es)):
        graphe.es[i]["pheromones"] = pherom_quantity


# Méthode qui génère un nombre de fourmis(agents) sur un graphe donné
# chaque fourmi s'attribuera une ville de départ (noeud) du graphe de
# façon aléatoire
# @param nb_fourmis : le nombre de fourmis à générer
# @param graphe : le graphe en question
# @return : le tableau de fourmis générées
def generate_fourmis(nb_fourmis, graphe):
    return [Agent(graphe) for x in range(nb_fourmis)]


# Méthode qui va faire bouger les fourmis jusqu'à ce qu'elles aient effecté
# leur circuit en ayant visité toutes les villes.
# Ensuite le backtracking est lancé, ce qui met
# à jour les compteurs de phéromones du graphe et permet de récupérer
# le meilleur chemin trouvé durant l'itération. Les mémoires des fourmis
# sont ensuites réinitialisées
# @param fourmis: les fourmis à déplacer
# @return : le coût du meilleur circuit trouvé durant l'itération
def iteration(fourmis, graphe):
    # lance le mouvement des fourmis autant de fois que d'arc
    # nécéssaire à parcourir
    # pour obtenir le circuit complet du graphe pour chaque fourmi
    for i in range(len(graphe.vs)-1):
        for fourmi in fourmis:
            fourmi.mouvement()

    # une fois que les fourmis ont atteint la fin de leur circuit, on lance
    # le backtracking-reset et on récupère le chemin et son coût minimum
    (best_chemin, chemin_cost) = fourmis[0].backtrack_n_reset()
    for i in range(1, len(fourmis)):
        (chemin_temp, cost_temp) = fourmis[i].backtrack_n_reset()
        if cost_temp < chemin_cost:
            (best_chemin, chemin_cost) = (chemin_temp, cost_temp)

    # retourne le chemin minimum
    return (best_chemin, chemin_cost)


# Méthode pour lancer le multi agents
# @param graphe : le graphe sur lequel on travaille
# @param nb_fourmis : le nombre de fourmis qui évolueront sur le graphe
# @param nb_itérations : nombre de fois que les fourmis parcoureront leur
#        circuit sur le graphe
# @param pherom_quantity : la quantité de départ de phéromones déposée
#        sur chaque arc du graphe
# @return : un 2-uplet contenant le meilleur chemin trouvé et le coût associé
def run(graphe, nb_fourmis, nb_iterations, pherom_quantity):
    max = 15
    setup_graphe(graphe, pherom_quantity, max)

    fourmis = generate_fourmis(nb_fourmis, graphe)

    (best_chemin, best_cost) = ([], 0)
    for i in range(nb_iterations):
        # calcul du meilleur chemin trouvé par les fourmis pour
        # l'itération courante
        (temp_chemin, temp_cost) = iteration(fourmis, graphe)

        # regarde si le chemin trouvé est meilleur qu'avant
        if len(best_chemin) == 0:
            (best_chemin, best_cost) = (temp_chemin, temp_cost)
        elif temp_cost < best_cost:
            (best_chemin, best_cost) = (temp_chemin, temp_cost)
        # print("Iteration n°"+str(i+1), best_chemin, best_cost)

    # print("\nMeilleur chemin :", best_chemin,
    # "avec un coût total de :", best_cost)
    return (best_chemin, best_cost)


if __name__ == "__main__":
    main()
