import igraph
from random import choice, randrange


class Graphe():

    def __init__(self, villes, alea=True):
        self.villes = villes
        self.alea = alea
        # generation du graphe igraph
        self.g = igraph.Graph.Full(len(villes))
        # initialisation
        self.init()


    def generate(nb_ville, alea=True, random_style = False):
        villes = []
        for i in range(nb_ville):
            villes.append(str(i))
            g = Graphe(villes, alea)

            if random_style:
                g.randomStyleVertex()

        return g

    def generateFromCityNameArray(villes_array):
        return Graphe(villes_array)

    def init(self):
        self.g.vs["name"] = self.villes
        self.g.vs["label"] = self.g.vs["name"]
        costs = []
        for i in range(len(self.g.vs)):
            if self.alea:
                costs.append(randrange(1, 100))
            else:
                costs.append(i+1)

        self.g.es["cost"] = costs

        self.hideAllEdge()
        self.hideAllLabel()
        # self.randomStyleVertex()
        # self.randomColorVertex()

    def hideAllEdge(self):
        for edge in self.g.es:
            edge["width"] = 0

    def showAllEdge(self):
        for edge in self.g.es:
            edge["width"] = 1

    def hideAllLabel(self):
        for vertex in self.g.vs:
            vertex["label_size"] = 0

    def showAllLabel(self):
        for vertex in self.g.vs:
            vertex["label_size"] = 15

    def randomStyleVertex(self):
        type = ["rectangle", "circle", "triangle-up", "triangle-down"]
        for vertex in self.g.vs:
            vertex["shape"] = choice(type)

    def randomColorVertex(self):
        color = ["blue", "pink", "green", "red"]
        for vertex in self.g.vs:
            vertex["color"] = choice(color)

    def visitEdge(self, index_v1, index_v2, color):
        edge_index = self.g.get_eid(index_v1, index_v2)
        edge = self.g.es[edge_index]
        edge["color"] = color
        edge["width"] = 5

    def setVertexColor(self, index, color):
        self.g.vs[index]["color"] = color
