#!/usr/bin/env python3
# coding: utf-8


from PyQt5.QtWidgets import QWidget, QHBoxLayout, \
    QVBoxLayout, QLabel, QAction, QMainWindow, \
    QPushButton, QSpinBox, QGridLayout, QCheckBox, \
    QTabWidget, QDoubleSpinBox
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import QThread, pyqtSignal
from graphwidget import GraphWidget
from tp2.genetique import Genetique
from tp_multi_agents import main as fourmis
import time
import sys
from graphe import Graphe


class AlgoThread(QThread):

    signal_fini = pyqtSignal(int, int)  # Coût du chemin, temps (ms)
    signal_en_cours = pyqtSignal(int, int)  # Nouveau couple

    def __init__(self, graphe, algo="genetique", **kwargs):
        # algo = genetique ou fourmis
        super().__init__()

        self.graphe = graphe
        self.algo = algo
        self.kwargs = kwargs

    def run(self):
        debut = time.time()
        if self.algo == "genetique":
            self.genetique = Genetique(self.graphe,
                                       taille_pop=self.kwargs["taille_pop"],
                                       temps_limite=self.kwargs["temps_max"],
                                       chance_croisement=self.kwargs["croisement"],
                                       chance_mutation=self.kwargs["mutation"])
            (resultat, cout) = self.genetique.algo()
        elif self.algo == "fourmis":
            (resultat, cout) = fourmis.run(self.graphe,
                                           self.kwargs["nb_fourmis"],
                                           self.kwargs["temps_max"], 1)
        else:
            raise ValueError("Algo {} non valide".format(self.algo))

        fin = time.time()
        
        self.signal_fini.emit(cout, int((fin - debut) * 1000))

        for idx in range(len(resultat)-1):
            first = resultat[idx]
            second = resultat[idx+1]
            self.signal_en_cours.emit(first, second)
            time.sleep(0.5)


class Window(QMainWindow):

    def __init__(self, graphe):
        super().__init__()

        self.graphe = graphe
        self.algo = "genetique"
        self.panelAlgorithme = self.genetiquePanel()

        self.algo_thread = AlgoThread(self.graphe.g, "fourmis")
        self.setupUI()

        self.algo_thread.signal_fini.connect(self.on_fini)
        self.algo_thread.signal_en_cours.connect(self.on_en_cours)

        # self.algo_thread.start()

    def setupUI(self):

        # parametre fenetre
        self.setGeometry(300, 300, 300, 220)
        self.setWindowTitle("IA2 -- Chemin Hamiltonien")

        self.widget = QWidget()
        self.setCentralWidget(self.widget)
        self.setWindowIcon(QIcon("icon.png"))

        # Menu bar
        self.setupMenu()

        self.layout = QHBoxLayout()
        self.gw = GraphWidget(self, self.graphe.g)

        self.layout.addWidget(self.panelAlgorithme)

        # table graphe et resultat
        self.tabWidget = QTabWidget()
        self.tabGraphe = QWidget()
        self.tabResultat = QWidget()
        # layout
        self.tabGraphe.layout = QHBoxLayout()
        self.tabResultat.layout = QVBoxLayout()
        self.tabGraphe.setLayout(self.tabGraphe.layout)
        self.tabResultat.setLayout(self.tabResultat.layout)

        # Add tabs
        self.tabWidget.addTab(self.tabGraphe, "Graphe")
        self.tabWidget.addTab(self.tabResultat, "Resultat")
        # add widget in tabs
        self.tabGraphe.layout.addWidget(self.gw)
        resultatWidget = self.resultatPanel()
        self.tabResultat.layout.addWidget(resultatWidget)

        self.layout.addWidget(self.tabWidget)
        self.widget.setLayout(self.layout)

        self.show()

    def setupMenu(self):
        # menu
        self.menuBar = self.menuBar()

        # sous menu file
        fileMenu = self.menuBar.addMenu('File')
        quitAction = QAction("Quit", self)
        quitAction.triggered.connect(self.close_application)
        fileMenu.addAction(quitAction)

        # sous menu Algo
        algoMenu = self.menuBar.addMenu('Algorithme')
        genetiqueAction = QAction("Genetique", self)
        genetiqueAction.triggered.connect(self.genetiqueMode)
        fourmisAction = QAction("Fourmis", self)
        fourmisAction.triggered.connect(self.fourmisMode)
        algoMenu.addAction(genetiqueAction)
        algoMenu.addAction(fourmisAction)

        # sous menu Algo
        grapheMenu = self.menuBar.addMenu('Graphe')
        genererAction = QAction("Generer un graphe", self)
        genererAction.triggered.connect(self.genererGrapheMode)
        grapheMenu.addAction(genererAction)

    def update(self):
        self.gw.update()

    def on_fini(self, cout, temps_ms):
        self.labelCout.setText("Coût: {}".format(cout))
        self.labelTemps.setText("Temps : {} ms".format(temps_ms))

    def on_en_cours(self, first, second):
        self.graphe.setVertexColor(first, "blue")
        self.graphe.setVertexColor(second, "green")
        self.graphe.visitEdge(first, second, "blue")
        self.update()

    def genetiqueMode(self):
        self.setPanelControl("genetique")

    def fourmisMode(self):
        self.setPanelControl("fourmis")

    def genererGrapheMode(self):
        self.setPanelControl("generer_graphe")

    def setPanelControl(self, name):
        # suppression ancien panel
        self.layout.removeWidget(self.panelAlgorithme)
        self.panelAlgorithme.deleteLater()

        # selon l'option choisie
        if name == "genetique":
            self.panelAlgorithme = self.genetiquePanel()
            self.algo = name
        elif name == "fourmis":
            self.panelAlgorithme = self.fourmisPanel()
            self.algo = name
        elif name == "generer_graphe":
            self.panelAlgorithme = self.genererGarphePanel()
        else:
            raise ValueError("Algo {} non valide".format(name))

        # insertion du nouveau panel
        self.layout.insertWidget(0, self.panelAlgorithme)

    def close_application(self):
        sys.exit()

    def genetiquePanel(self):
        # Panel
        genetiqueWidget = QWidget()
        genetiquePanelWidget = QWidget()
        layoutGenetique = QGridLayout()
        layoutPanelGenetique = QVBoxLayout()
        layoutGenetique.setSpacing(10)

        # Titre
        labelTitre = QLabel("Algorithme genetique")

        # element de saisie
        labelTaille = QLabel("Taille de la population:")
        labelTemps = QLabel("Temps maximum:")
        labelChanceCroisement = QLabel("Chance de croisement:")
        labelChanceMutation = QLabel("Chance de mutation:")

        self.genetique_editTaille = QSpinBox()
        self.genetique_editTaille.setMinimum(2)
        self.genetique_editTaille.setMaximum(200)

        self.genetique_editTemps = QSpinBox()
        self.genetique_editTemps.setMinimum(10)
        self.genetique_editTemps.setMaximum(1000)

        self.genetique_editChanceCroisement = QDoubleSpinBox()
        self.genetique_editChanceCroisement.setMaximum(1)

        self.genetique_editChanceMutation = QDoubleSpinBox()
        self.genetique_editChanceMutation.setMaximum(1)

        # button validation
        button = QPushButton('Lancer', self)
        button.clicked.connect(self.run_algo_genetique)

        # Ajout des elements dans le label
        layoutGenetique.addWidget(labelTaille, 1, 0)
        layoutGenetique.addWidget(self.genetique_editTaille, 1, 1)

        layoutGenetique.addWidget(labelTemps, 2, 0)
        layoutGenetique.addWidget(self.genetique_editTemps, 2, 1)

        layoutGenetique.addWidget(labelChanceCroisement, 3, 0)
        layoutGenetique.addWidget(self.genetique_editChanceCroisement, 3, 1)

        layoutGenetique.addWidget(labelChanceMutation, 4, 0)
        layoutGenetique.addWidget(self.genetique_editChanceMutation, 4, 1)

        # add bouton
        layoutPanelGenetique.addStretch(1)
        layoutPanelGenetique.addWidget(labelTitre)
        layoutPanelGenetique.addWidget(genetiqueWidget)
        layoutPanelGenetique.addWidget(button)
        layoutPanelGenetique.addStretch(1)

        genetiqueWidget.setLayout(layoutGenetique)
        genetiquePanelWidget.setLayout(layoutPanelGenetique)

        return genetiquePanelWidget

    def fourmisPanel(self):
        # Panel
        fourmisWidget = QWidget()
        fourmisPanelWidget = QWidget()
        layoutFourmis = QGridLayout()
        layoutPanelFourmis = QVBoxLayout()
        layoutFourmis.setSpacing(10)

        # Titre
        labelTitre = QLabel("Algorithme Colonie de fourmis")

        # element de saisi
        labelTaille = QLabel("Nombre de fourmis:")
        labelIteration = QLabel("Nombre d'iteration:")

        self.fourmis_editTaille = QSpinBox()
        self.fourmis_editTaille.setMinimum(1)

        self.fourmis_editIteration = QSpinBox()
        self.fourmis_editTaille.setMinimum(10)

        # button validation
        button = QPushButton('Lancer', self)
        button.clicked.connect(self.run_algo_fourmis)

        # Ajout des element dans le label
        layoutFourmis.addWidget(labelTaille, 1, 0)
        layoutFourmis.addWidget(self.fourmis_editTaille, 1, 1)

        layoutFourmis.addWidget(labelIteration, 2, 0)
        layoutFourmis.addWidget(self.fourmis_editIteration, 2, 1)

        # add boutton
        layoutPanelFourmis.addStretch(1)
        layoutPanelFourmis.addWidget(labelTitre)
        layoutPanelFourmis.addWidget(fourmisWidget)
        layoutPanelFourmis.addWidget(button)
        layoutPanelFourmis.addStretch(1)

        fourmisWidget.setLayout(layoutFourmis)
        fourmisPanelWidget.setLayout(layoutPanelFourmis)

        return fourmisPanelWidget

    def genererGarphePanel(self):
        # Panel
        grenererGrapheWidget = QWidget()
        genererGraphePanelWidget = QWidget()

        # layout
        layoutGenererGraphe = QGridLayout()
        layoutPanelGenererGraphe = QVBoxLayout()
        layoutGenererGraphe.setSpacing(10)

        # Titre
        labelTitre = QLabel("Generer un graphe")

        # element de saisi
        labelTaille = QLabel("Nombre de noeuds :")
        self.generer_editTaille = QSpinBox()
        self.generer_checkBoxAleatoire = QCheckBox('Couts aleatoire', self)
        self.generer_checkBoxStyleAleatoire = QCheckBox('Style vertex aleatoire', self)

        # button validation
        button = QPushButton('Generer', self)
        button.clicked.connect(self.generate_graph)

        # Ajout des element dans le label
        layoutGenererGraphe.addWidget(labelTaille, 1, 0)
        layoutGenererGraphe.addWidget(self.generer_editTaille, 1, 1)
        layoutGenererGraphe.addWidget(self.generer_checkBoxAleatoire, 2, 0)
        layoutGenererGraphe.addWidget(self.generer_checkBoxStyleAleatoire, 3, 0)

        # add boutton
        layoutPanelGenererGraphe.addStretch(1)
        layoutPanelGenererGraphe.addWidget(labelTitre)
        layoutPanelGenererGraphe.addWidget(grenererGrapheWidget)
        layoutPanelGenererGraphe.addWidget(button)
        layoutPanelGenererGraphe.addStretch(1)

        grenererGrapheWidget.setLayout(layoutGenererGraphe)
        genererGraphePanelWidget.setLayout(layoutPanelGenererGraphe)

        return genererGraphePanelWidget

    def resultatPanel(self):
        # Panel
        resultatWidget = QWidget()

        # layout
        layoutResultat = QVBoxLayout()

        # element de saisi
        self.labelCout = QLabel("Cout : ?")
        self.labelTemps = QLabel("Temps : ? ms")

        # Ajout des element dans le label
        layoutResultat.addWidget(self.labelCout)
        layoutResultat.addWidget(self.labelTemps)
        resultatWidget.setLayout(layoutResultat)

        return resultatWidget

    def run_algo_genetique(self):
        self.resetGraphStyle()
        taille_pop = self.genetique_editTaille.value()
        temps_max = self.genetique_editTemps.value()
        croisement = self.genetique_editChanceCroisement.value()
        mutation = self.genetique_editChanceMutation.value()

        self.algo_thread.kwargs = {
            "taille_pop": taille_pop,
            "temps_max": temps_max,
            "croisement": croisement,
            "mutation": mutation
        }
        self.algo_thread.algo = "genetique"
        self.algo_thread.graphe = self.graphe.g

        self.algo_thread.start()

    def run_algo_fourmis(self):
        self.resetGraphStyle()
        nb_fourmis = self.fourmis_editTaille.value()
        temps_max = self.fourmis_editIteration.value()

        self.algo_thread.kwargs = {
            "nb_fourmis": nb_fourmis,
            "temps_max": temps_max
        }
        self.algo_thread.algo = "fourmis"
        self.algo_thread.graphe = self.graphe.g

        self.algo_thread.start()

    def generate_graph(self):
        taille = self.generer_editTaille.value()
        alea = self.generer_checkBoxAleatoire.isChecked()
        styleAleatoire = self.generer_checkBoxStyleAleatoire.isChecked()
        
        self.graphe = Graphe.generate(taille, alea,styleAleatoire)
        self.gw.set_graph(self.graphe.g)
        self.gw.update()

    def resetGraphStyle(self):
        self.graphe.hideAllEdge()
        for vertex in self.graphe.g.vs:
            vertex["color"] = "red"
