from PyQt5.QtWidgets import QLabel
from PyQt5.QtGui import QPixmap
from PyQt5.QtCore import QThread, pyqtSignal
import igraph
from tempfile import NamedTemporaryFile
import os


class GraphWidgetPlotter(QThread):
    signal_fini = pyqtSignal(str)

    def __init__(self, graph):
        super().__init__()
        self.graph = graph

    def run(self):
        tmpfile = NamedTemporaryFile(suffix=".png", delete=False)
        # print(tmpfile.name)
        graph_layout = self.graph.layout("circular")
        igraph.plot(self.graph, tmpfile.name,
                    layout=graph_layout,
                    margin=(50, 20, 50, 20))

        self.signal_fini.emit(tmpfile.name)


class GraphWidget(QLabel):
    def __init__(self, parent, graph):
        super().__init__(parent)
        self.parent = parent
        self.graph = graph
        self.generatedFile = []
        self.pix = QPixmap()

        self.harry_plotter = GraphWidgetPlotter(self.graph)
        self.harry_plotter.signal_fini.connect(self.on_fini)

        self.update()

    def update(self):
        self.harry_plotter.start()

    def set_graph(self, g):
        self.graph = g
        self.harry_plotter.graph = g

    def on_fini(self, path):
        self.pix.load(path)
        self.setPixmap(self.pix)
        self.generatedFile.append(path)

        # if linux plateform
        if os.name == "posix":
            os.remove(path)
