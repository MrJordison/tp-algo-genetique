from .individu import Individu
from random import random


class Population:

    def __init__(self, nb_noeuds, taille, longevite, init=True):
        self.individus = []

        self.longevite = longevite

        if init:
            for i in range(taille):
                self.individus.append(Individu(nb_noeuds))

    def tuer(self):
        # tuer les individu qui sont trop vieux
        for individu in self.individus:
            if individu.age >= self.longevite:
                individu.mort()
                del individu

    def croisement(self):
        # TODO: croisement
        pass

    def vieillir(self):
        for individu in self.individus:
            individu.age += 1

    def mutation(self):
        # TODO: mutation
        pass

    def fini(self):
        return False

    def get_individu_roulette(self):
        if len(self.individus) == 0:
            return None

        # https://en.wikipedia.org/wiki/Fitness_proportionate_selection#Java_-_linear_O.28n.29_version
        fitness_sum = 0
        for i in self.individus:
            if i.valuation > 0:
                fitness_sum += i.valuation

        rand_value = random() * fitness_sum
        for i in self.individus:
            if i.valuation > 0:
                rand_value -= i.valuation
                if rand_value <= 0:
                    return i

        # Au cas où y'ai eu un petit problème de rounding...
        # On renvoie le dernier
        return self.individus[-1]

    def get_best(self):
        val_max = -1
        val_max_id = -1

        for (i, individu) in enumerate(self.individus):
            if individu.valuation > val_max:
                val_max = individu.valuation
                val_max_id = i

        return self.individus[val_max_id]

    def __str__(self):
        return "Individus : " + ", ".join(str(e) for e in self.individus)
