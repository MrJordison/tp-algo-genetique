# coding: utf-8

from random import random, randrange
from .population import Population
import igraph
import sys


class Genetique:

    def __init__(self, graph, taille_pop=20, temps_limite=100,
                 longevite=5, chance_croisement=0.7,
                 chance_mutation=0.001):
        self.graph = graph

        self.taille_pop = taille_pop
        self.temps_limite = temps_limite
        self.longevite = longevite
        self.chance_croisement = chance_croisement
        self.chance_mutation = chance_mutation

    def algo(self):
        # On crée une population random
        self.population = Population(len(self.graph.vs),
                                     taille=self.taille_pop,
                                     longevite=self.longevite)
        temps = 0
        self.meilleure_valuation = -1
        self.meilleure_valuation_age = 0

        # Boucle principale
        while not self.fini() and (temps < self.temps_limite):
            self.valuer_pop()

            temps += 1
            # self.population.vieillir()
            # self.population.tuer()

            nouvelle_pop = Population(len(self.graph.vs), self.taille_pop,
                                      self.longevite, init=False)
            best = self.population.get_best()

            if best.valuation > self.meilleure_valuation:
                self.meilleure_valuation = best.valuation
                self.meilleure_valuation_age = 0
            else:
                self.meilleure_valuation_age += 1

            nouvelle_pop.individus.append(best)
            self.population.individus.remove(best)

            while len(self.population.individus) > 0:
                first_parent = self.population.get_individu_roulette()
                self.population.individus.remove(first_parent)

                second_parent = self.population.get_individu_roulette()
                if second_parent is None:
                    # Alors il ne reste plus qu'un individu,
                    # on l'ajoute et on termine
                    first_parent.mutation(self.chance_mutation)
                    nouvelle_pop.individus.append(first_parent)
                    break
                self.population.individus.remove(second_parent)

                first = None
                second = None
                rnd = random()
                if rnd < self.chance_croisement:
                    children = first_parent.croisement(second_parent)
                    first, second = children
                else:
                    first, second = first_parent, second_parent

                first.mutation(self.chance_mutation)
                second.mutation(self.chance_mutation)
                nouvelle_pop.individus.append(first)
                nouvelle_pop.individus.append(second)

            self.population = nouvelle_pop
        self.valuer_pop()

        best = self.population.get_best()
        return (best.valeur, self.cout(best))

    def fitness(self, individu):
        # TODO: faire ça mieux
        return sys.maxsize - self.cout(individu)

    def cout(self, individu):
        cost = 0
        for idx in range(len(individu.valeur)-1):
            one = individu.valeur[idx]
            two = individu.valeur[idx+1]
            edge = self.get_edge_between(one, two)

            if edge is None:
                return -1

            cost += edge["cost"]
        return cost

    def fini(self):
        # On termine quand la valuation n'a pas changé depuis 40 tours
        return self.meilleure_valuation_age >= 40

    def get_edge_between(self, v1, v2):
        for e in self.graph.es:
            t = e.tuple
            if (t[0] == v1 and t[1] == v2) or (t[0] == v2 and t[1] == v1):
                return e
        return None

    def valuer_pop(self):
        for individu in self.population.individus:
            if individu.valuation is None:
                individu.valuation = self.fitness(individu)
