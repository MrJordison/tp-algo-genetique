# coding: utf-8

# Fichier de configuration pour l'algorithme génétique
# Ok je change un peu pour plus correspondre à ce qui se fait vraiment en algo génétique

longevite = 5
population_initiale = 20
#nb_croisements = 6
#nb_mutations = 3
chance_croisement = 0.7
chance_mutation = 0.001
temps_limite = 100

# Configuration spécifique à l'agorithme de recherche de chemin hamiltonien
nb_noeuds = 20
