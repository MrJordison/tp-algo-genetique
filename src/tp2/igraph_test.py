#!/usr/bin/env python3
# coding: utf-8

import igraph

g = igraph.Graph()

g.add_vertices(2)
g.vs["name"] = ["Limoges","Gueret","Tulle",
                "Poitiers","Niort","La Rochelle","Angouleme",
                "Bordeaux","Perigeux","Agen","Mont-de-Marsant","Pau"]
g.vs["label"] = g.vs["name"]

g.add_edges([(0, 1)])
g.es[0]["cost"] = 100

layout = g.layout_kamada_kawai()
igraph.plot(g, layout=layout)
