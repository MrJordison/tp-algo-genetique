# Représentation d'un individu (solution possible)

Liste de longueur n (nombre de noeuds), chaque élément est un nombre
entre 0 et n-1 unique indiquant la position du noeud dans le chemin.

# Autre

Chaque noeud du graphe sera représenté par 5 bits, donc 32 noeuds max (0-31)
Un chromosome est composé de n * 5 bits (n étant le nombre de noeuds du graphe)
L'ordre d'écriture des noeuds représente l'ordre dans lequel on choisit de parcourir le graphe

Exemple :

00010 00000 00001

  2     0     1
  
On parcourt d'abord le noeud 2, puis 0, et enfin 1
