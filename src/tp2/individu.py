# coding: utf-8
from random import randrange, random, shuffle


class Individu:

    def __init__(self, nb_noeuds, valeur=None):
        self.nb_noeuds = nb_noeuds
        if valeur is None:
            valeur = self.valeur_random()

        self.valeur = valeur
        self.age = 0
        self.valuation = None

    def mort(self):
        # print "mort de l'individu"
        pass

    def croisement(self, individu):
        # Pour croiser, on prend la première partie et on
        # complète avec le deuxième
        pivot = randrange(len(self.valeur))
        first = self.valeur[:pivot]
        for val in individu.valeur:
            if val not in first:
                first.append(val)

        second = individu.valeur[:pivot]
        for val in individu.valeur:
            if val not in second:
                second.append(val)

        # print("Croisement. moi=", self.valeur, " // autre=", individu.valeur)
        return [Individu(self.nb_noeuds, first),
                Individu(self.nb_noeuds, second)]

    def mutation(self, chance):
        # On passe sur chaque valeur avec une chance de swap avec
        # celui d'avant

        for idx in range(1, len(self.valeur)):
            rnd = random()
            if rnd < chance:
                tmp = self.valeur[idx-1]
                self.valeur[idx-1] = self.valeur[idx]
                self.valeur[idx] = tmp

    def valeur_random(self):
        values = list(range(self.nb_noeuds))
        shuffle(values)
        return values

    def __str__(self):
        return str(self.valeur)

    def __eq__(self, other):
        return self.valeur == other.valeur and self.age == other.age
