#!/usr/bin/env python3

# tp2 - algorithme génétique pour trouver un chemin hamiltonien dans un graphe

from PyQt5.QtWidgets import QApplication
from window import Window
import sys
from graphe import Graphe
from tp2 import config

if __name__ == "__main__":
    app = QApplication(sys.argv)

    villes = ["Limoges", "Gueret", "Tulle",
              "Poitiers", "Niort", "La Rochelle", "Angouleme",
              "Bordeaux", "Perigeux", "Agen", "Mont-de-Marsant", "Pau"]

    graphe = Graphe.generate(config.nb_noeuds)

    w = Window(graphe)

    sys.exit(app.exec_())
