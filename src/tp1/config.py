# coding: utf-8

# Fichier de configuration pour l'algorithme génétique

longevite = 5
population_initiale = 20
nb_croisements = 6
nb_mutations = 3

# Configuration spécifique à l'agorithme de recherche de nombre

nb_min = 0
nb_max = 100
