from random import randrange
import config


class Individu:

    def __init__(self, valeur=None):
        if valeur is None:
            valeur = randrange(config.nb_min, config.nb_max+1)

        self.valeur = valeur
        self.age = 0
        self.valuation = None

    def mort(self):
        # print "mort de l'individu"
        pass

    def croisement(self, individu):
        moyenne = (individu.valeur + self.valeur)/2
        return Individu(moyenne)

    def __str__(self):
        return "Valeur : " + str(self.valeur)
