# coding: utf-8

from random import randrange
import config
from population import Population


class Genetique:

    def __init__(self):
        self.nombre = randrange(config.nb_min, config.nb_max+1)

    def algo(self):
        self.population = Population()
        temps = 0

        # TODO: Extraire le temps limite dans une variable de config
        while not self.population.fini() and (temps < 20):
            temps += 1
            self.population.vieillir()
            self.population.tuer()

            for i in range(config.nb_croisements):
                self.population.croisement()

            for i in range(config.nb_mutations):
                self.population.mutation()

            self.valuer_pop()

        if self.population.fini():
            print("On a trouvé une solution en %d générations !" % (temps))
            print("Il y a %d individus dans la population" %
                  (len(self.population.individus)))
            print("Le nombre était " + str(self.nombre))
            print(str(self.population))

    def fitness(self, individu):
        return abs(self.nombre - individu.valeur)

    def valuer_pop(self):
        for individu in self.population.individus:
            if individu.valuation is None:
                individu.valuation = self.fitness(individu)
