import config
from individu import Individu
from random import randrange


class Population:

    def __init__(self):
        self.individus = []

        for i in range(config.nb_min, config.nb_max+1):
            self.individus.append(Individu())

    def tuer(self):
        # tuer les individu qui sont trop vieux
        for individu in self.individus:
            if individu.age >= config.longevite:
                individu.mort()
                del individu

    def croisement(self):
        for i in range(config.nb_croisements):
            individu_1 = self.individus[randrange(0, len(self.individus))]
            individu_2 = self.individus[randrange(0, len(self.individus))]
            enfant = individu_1.croisement(individu_2)
            self.individus.append(enfant)

    def vieillir(self):
        for individu in self.individus:
            individu.age += 1

    def mutation(self):
        individu_mute = self.individus[randrange(0, len(self.individus))]
        individu_mute.valeur = randrange(config.nb_min, config.nb_max+1)

    def fini(self):
        for i in self.individus:
            if i.valuation == 0:
                return True
        return False

    def __str__(self):
        return "Individus : " + ", ".join(str(e) for e in self.individus)
